public class Kettle {
	public String colour;
	public int quantity;
	public String brand;
	
	public void printBrand(String brand) {
		System.out.println(brand + " goes brrrrrrrrrrr");
	}
	public void printQuantity(int quantity) {
		System.out.println("The kettle can hold a maximum of " + quantity + " ml");
	}
}